**Baryon** is a dark [ProtonMail](https://mail.protonmail.com/) theme.

Copr. 2019 [Avanier](https://joshavanier.github.io). [MIT](LICENSE).
